const webpack = require("webpack");
const path = require('path');//引入path模块
function resolve(dir){
  return path.join(__dirname,dir)//path.join(__dirname)设置绝对路径
}
module.exports = {
    //配置路径简写
    outputDir: 'dist',
    //配置路径别名
    chainWebpack:(config)=>{
    config.resolve.alias
      .set('@',resolve('./src'))
      .set('components',resolve('./src/components'))
      .set('assets',resolve('./src/assets'))
      .set('views',resolve('./src/views'))
    //set第一个参数：设置的别名，第二个参数：设置的路径

  },
    //使用boostrap，将其部署到jquery的环境下就可直接使用
    configureWebpack:{
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
        Popper: ["popper.js", "default"]
      })
    ]},
    //配置跨域
    devServer: {
    proxy: {
      // before:require('./src/mock/index'),
      '/api': {
        target: 'http://localhost:8443', //对应服务器地址
        changeOrigin: true, //允许跨域
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
};

