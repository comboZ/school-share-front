module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  //借助插件 babel-plugin-import可以实现按需加载组件，减少文件体积。
  // plugins: [["import", {
  //   "libraryName": "view-design",
  //   "libraryDirectory": "src/components"
  // }]]
};
