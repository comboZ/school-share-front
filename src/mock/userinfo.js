import Mock from 'mockjs'
//模拟增删改查用户信息
//获取用户信息
const data={
  "extend":{
    "user": {
      "userId": '0001 ',
      "userName": '小明',
      "userMajor":{ "majorName": '软件工程'},
      "userNickName": '听说名字越长越好',
      "userPassword": '111111',
      "userIntroduce": '我是一个猪头',
      "userEmail": '235778444@qq.com'
    }
  },
};
Mock.mock('http://localhost:8443/api/userinfo', 'get', data);

//修改用户信息，即将已有的信息发送过去，再替换成新的信息，再次渲染
//首先我要将信息发送过去-这里要能获取信息（）
