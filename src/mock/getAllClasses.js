//这里主要是获取某个部门下所有的课程信息
import Mock from 'mockjs'

//获取所有院系信息
const classes = [
  {id:1,name:"高等数学"},
  {id:2,name:"bbb"},
  {id:3,name:"vvvv"},
  {id:4,name:"ddddddddd"},
];
const data = { "classes": classes};
Mock.mock(RegExp('http://localhost:8443/api/getClassesByDepartId' + '.*'),"get",data);
