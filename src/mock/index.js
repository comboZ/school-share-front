import Mock from 'mockjs'
require('./login')
require('./userinfo')
require('./forumTest')
require('./getAllClasses')
const data={
  "id":"@guid",
  "name":"@cname",
};
  Mock.mock('http://localhost:8443/api/test', 'get', data);

export default
  Mock;
