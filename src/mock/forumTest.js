//这里主要是测试论坛的信息处理
import Mock from 'mockjs'

//获取所有院系信息
const departments = [
  {id:1,name:"美术与设计学院"},
  {id:2,name:"工商管理学院"},
  {id:3,name:"电子信息工程学院"},
  {id:4,name:"计算机学院"},
  {id:5,name:"旅游学院"},
  {id:6,name:"机械工程学院"},
  {id:7,name:"文学院"},
  {id:8,name: "公共管理学院"},
  {id:9,name: "外国语学院"},
  {id:10,name:"药学与食品科学学院"},
  {id:11,name:"化工与新能源材料学院"},
  {id:12,name: "建筑与城乡规划学院"},
  {id:13,name:"金融与贸易学院"},
  {id:14,name:"物流管理与工程学院"},
  {id:15,name:"音乐舞蹈学院"},
  {id:16,name:"健康学院"},
  {id:17,name:"航空工程学院"},
  {id:18,name:"公共外语教育学院"},
  {id:19,name:"公共基础与应用统计学院"},
  {id:20,name:"创新创业学院"},
  {id:21,name:"马克思主义学院"},
  {id:22,name:"体育科学学院"},
  {id:23,name:"继续教育学院"}
];
const data = { "departments": departments};
Mock.mock('http://localhost:8443/api/getDepartments', 'get', data);
