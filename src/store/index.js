import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    //存储登录用户信息
    user: {
      userId: window.localStorage.getItem('user' || '[]') == null ? '' : JSON.parse(window.localStorage.getItem('user' || '[]')).userId
    },
    // 保持面包屑状态
    breadListState:[
      {name:'',path:'/'}
    ]
  },
  mutations: {
    //登录存储
    login (state, user) {
      state.user = user;
      window.localStorage.setItem('user', JSON.stringify(user))
    },
    // 面包屑导航
    breadListStateAdd(state,obj){
      state.breadListState.push(obj);
    },
    breadListStateRemove(state,num){
      state.breadListState=state.breadListState.slice(0,num);
    }
  }
})
