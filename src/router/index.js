import Vue from 'vue'
import VueRouter from 'vue-router'

const Login = () => import('views/login/Login');
const Profile = () => import('views/profile/Profile');
const SeleInfo =() => import('components/content/self-info/SelfInfo');
const MyTimeTable =() => import('components/content/my-timetable/MyTimeTable');
const MyInfo =() => import('components/content/my-info/Myinfo');
const MyHistory =() => import('components/content/myhistory/Myhistory');
const Secret =() => import('components/content/secret/Secret');
const ForumMain =() => import('components/content/forum/forum-main');
const Moderator =() => import('components/content/moderator/moderator');
const MyBoutique =() => import('components/content/my-post/my-boutique/MyBoutique');
const MyPostAll =() => import('components/content/my-post/my-post-all/MyPost');
const MyReply =() => import('components/content/my-post/my-reply/MyReply');

Vue.use(VueRouter);

const routes = [
   //默认跳转
  {
    path: '',
    redirect: '/login'
  },
  //登录界面跳转
  {
    path: '/login',
    component: Login
  },
  {
    path:'/profile',
    component:Profile,
    // meta: {
    //   requireAuth: true
    // },
    children:[
      //我的个人信息
      {
        path: 'selfinfo',
        component: SeleInfo,
        name: '我的基本资料'
      },
      //我的个人隐私
      {
        path: 'secret',
        component: Secret,
        name: '隐私设置'
      },
      //我的通知
      {
        path: 'myinfo',
        component: MyInfo,
        name: '我的通知'
      },
      //我的课程表
      {
        path: 'mytimetable',
        component: MyTimeTable,
        name: '我的课表'
      },
      //我的痕迹
      {
        path: 'myhistory',
        component: MyHistory,
        name: '我的痕迹'
      },
      //论坛主页
      {
        path: 'forum_main',
        component: ForumMain,
        name: '论坛首页'
      },
      //版主设置
      {
        path: 'moderator',
        component: Moderator,
        name: '版主设置'
      },
      //我的发布
      {
        path: 'my_post_all',
        component: MyPostAll,
        name: '我的发布'
      },
      //我的精品
      {
        path: 'my_boutique',
        component: MyBoutique,
        name: '我的精品'
      },
      //我的回复
      {
        path: 'my_reply',
        component: MyReply,
        name: '我的回复'
      }

    ]
  }
];
const router = new VueRouter({
  routes,
  mode: 'history'
});

export default router
