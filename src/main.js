import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import axios from  'axios'
import VueParticles from './vue-particles'
import VuePrism from 'vue-prism'
import store from './store'
import  "bootstrap"
import "bootstrap/dist/css/bootstrap.css"
// require('./mock');

Vue.config.productionTip = false;
Vue.use(ViewUI);
Vue.use(VueParticles, VuePrism);


axios.defaults.withCredentials = true;
axios.defaults.baseURL = 'http://localhost:8443/api';
axios.defaults.timeout = 5000;
Vue.config.productionTip = false;
Vue.prototype.$axios = axios;

router.beforeEach((to, from, next) => {
    if (to.meta.requireAuth) {
      if (store.state.user.username) {
        next()
      } else {
        next({
          path: 'login',
          //query: {redirect: to.fullPath}
        })
      }
    } else {
      next()
    }
  }
);


new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app');
